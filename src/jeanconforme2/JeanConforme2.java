/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeanconforme2;

import java.util.Scanner;

/**
import javax.swing.JOptionPane;

/**
 *
 * @author Jean
 */
public class JeanConforme2 {
    /**
     * @param args the command line arguments
     */
    /**
    public static void main(String[] args) {
        // TODO code application logic here
     int tabla[];
     int mayor=0;
     int tam;
     
     tam=Integer.parseInt(JOptionPane.showInputDialog("Introduzca el total de numeros"));
     tabla=new int[tam];
     for(int i=0; i<tam; i++)
     {
         tabla[i]=Integer.parseInt(JOptionPane.showInputDialog("Introduce el numero"+(i+1)));
         if(tabla[i]>mayor)mayor=tabla[i];
     }
        JOptionPane.showMessageDialog(null,"El numero mayor es:"+mayor);
    }}
*/
/**
 *
 * @author jean
 */
    /**
     * @param args the command line arguments
     */
    /**
    public static void main(String[] args) {
        // TODO code application logic here
       Scanner teclado = new Scanner (System.in);
       System.out.print("Por favor ingrese el tamaño del cuadro:");
       int n= teclado.nextInt();
       if (n>=0 && n<50){
           //linea superior
           for (int i=0; i<n;i++){
               System.out.print("*");
           }
           System.out.println();
           
           //lados
           for (int i=0;i<n-2;i++){
               System.out.print("*");
               for (int j=0;j<n-2;j++){
                   System.out.print(" ");
                }
               System.out.println("*");
           }
           //linea inferior
           for (int i=0; i<n;i++){
               System.out.print("*");
           
    }
    }else {
           System.out.println("Error.El dato tiene que estar entre o a 50");
       }
    }}
*/
/**
 *
 * @author jean
 */
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner piramide= new Scanner(System.in);
        System.out.print("Ingrese la cantidad de filas: ");
        int filas=piramide.nextInt();
        piramide.close();
        
        System.out.println();
        for (int alto=1; alto <= filas; alto++){
            //espacios
            for (int espacio=1; espacio <= filas-alto; espacio++){
                System.out.print(" ");
            }
            //asteriscos
            for (int asterisco=1; asterisco <= (alto*2)-1; asterisco++){
                System.out.print("*");
            }
            System.out.println();
        }}}